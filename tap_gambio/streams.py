"""Stream type classes for tap-gambio."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable
from xml.dom.minicompat import StringTypes

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_gambio.client import gambioStream
import requests
# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class OrderList(gambioStream):
    """Define custom stream."""
    name = "orders"
    path = "/orders"
    records_jsonpath = "$[*]"
    primary_keys = ["id"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(

        th.Property("id", th.IntegerType),
        th.Property("statusId", th.IntegerType),
        th.Property("statusName", th.StringType),
        th.Property("totalSum", th.StringType),
        th.Property("purchaseDate", th.StringType),
        th.Property("comment", th.StringType),
        th.Property("withdrawalIds", th.ArrayType(
            th.IntegerType
        )),
        th.Property("mailStatus", th.BooleanType),
        th.Property("customerId", th.IntegerType),
        th.Property("customerName", th.StringType),
        th.Property("customerEmail", th.StringType),
        th.Property("customerStatusId", th.IntegerType),
        th.Property("customerStatusName", th.StringType),
        # th.Property("customerMemos", th.IntegerType),
        th.Property("deliveryAddress", th.ObjectType(

            th.Property("firstName", th.StringType),
            th.Property("lastName", th.StringType),
            th.Property("company", th.StringType),
            th.Property("street", th.StringType),
            th.Property("houseNumber", th.StringType),
            th.Property("additionalAddressInfo", th.StringType),
            th.Property("postcode", th.StringType),
            th.Property("city", th.StringType),
            th.Property("state", th.StringType),
            th.Property("country", th.StringType),
            th.Property("countryIsoCode", th.StringType),
            th.Property("test", th.IntegerType),
            th.Property("test", th.IntegerType),
            th.Property("test", th.IntegerType),

        )),
        th.Property("billingAddress", th.ObjectType(

            th.Property("firstName", th.StringType),
            th.Property("lastName", th.StringType),
            th.Property("company", th.StringType),
            th.Property("street", th.StringType),
            th.Property("houseNumber", th.StringType),
            th.Property("additionalAddressInfo", th.StringType),
            th.Property("postcode", th.StringType),
            th.Property("city", th.StringType),
            th.Property("state", th.StringType),
            th.Property("country", th.StringType),
            th.Property("countryIsoCode", th.StringType),
            th.Property("test", th.IntegerType),
            th.Property("test", th.IntegerType),
            th.Property("test", th.IntegerType),

        )),
        th.Property("paymentType", th.ObjectType(
            
            th.Property("title", th.StringType),
            th.Property("module", th.StringType),

        )),
        th.Property("shippingType", th.ObjectType(
            
            th.Property("title", th.StringType),
            th.Property("module", th.StringType),

        )),
        th.Property("trackingLinks", th.ArrayType(
            th.StringType
        )),
        th.Property("_links", th.ObjectType(
            th.Property("customer", th.StringType),
            th.Property("status", th.StringType),
            th.Property("tracking_codes", th.StringType),
        ))     
       
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "order_id": record["id"],
        }

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if previous_token is None:
            # If previous_token is None, we only got the first page so we should request page 2
            return 2

        # Parse response as JSON
        res = response.json()
        if len(res) == 0:
            # If this page was empty, we are done querying
            return None
        
        # time.sleep(1)
        # Else, query next page
        return previous_token + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""

        params: dict = {}
        if next_page_token is None:
            params["page"] = 1
        else:
            params["page"] = next_page_token

        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key

        params["per_page"] = 100
        
        return params


class OrderItemsStream(gambioStream):
    """Define custom stream."""
    name = "ordersitems"
    path = "/orders/{order_id}"
    records_jsonpath = "$[*]"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = OrderList
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return None
    
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("order_id", th.IntegerType),
        th.Property("statusId", th.IntegerType),
        th.Property("purchaseDate", th.StringType),
        th.Property("currencyCode", th.StringType),

        th.Property("items", th.ArrayType(
            th.ObjectType(

        th.Property("id", th.IntegerType),
        th.Property("model", th.StringType),
        th.Property("name", th.StringType),
        th.Property("quantity", th.NumberType),
        th.Property("price", th.NumberType),
        th.Property("finalPrice", th.NumberType),
        th.Property("tax", th.IntegerType),
        th.Property("isTaxAllowed", th.BooleanType),
        th.Property("discount", th.IntegerType),
        th.Property("shippingTimeInformation", th.StringType),
        th.Property("checkoutInformation", th.StringType),
        th.Property("quantityUnitName", th.StringType),
        th.Property("downloadInformation", th.ArrayType(
                th.StringType
        )),
        # th.Property("gxCustomizerData", th.ArrayType(
        #         th.StringType
        # )),
        th.Property("addonValues", th.ObjectType(

            th.Property("productId", th.StringType),
            th.Property("productType", th.StringType),
            th.Property("identifier", th.StringType)

        ))
        )))
    ).to_dict()




class ProductStream(gambioStream):

    """Define custom stream."""
    name = "products"
    path = "/products"
    records_jsonpath = "$[*]"
    primary_keys = ["id"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    
    schema = th.PropertiesList(

        th.Property("id", th.IntegerType),
        th.Property("isActive", th.BooleanType),
        th.Property("sortOrder", th.IntegerType),
        th.Property("dateAdded", th.StringType),
        th.Property("dateAvailable", th.StringType),
        th.Property("lastModified", th.StringType),
        th.Property("orderedCount", th.IntegerType),
        th.Property("productModel", th.StringType),
        th.Property("ean", th.StringType),
        th.Property("price", th.NumberType),
        th.Property("discountAllowed", th.IntegerType),
        th.Property("taxClassId", th.IntegerType),
        th.Property("quantity", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("image", th.StringType),
        th.Property("imageAltText", th.StringType),
        th.Property("urlKeywords", th.StringType),
        th.Property("weight", th.NumberType),
        th.Property("shippingCosts", th.NumberType),
        th.Property("shippingTimeId", th.IntegerType),
        th.Property("productTypeId", th.IntegerType),
        th.Property("manufacturerId", th.IntegerType),
        th.Property("quantityUnitId", th.IntegerType),
        th.Property("isFsk18", th.BooleanType),
        th.Property("isVpeActive", th.BooleanType),
        th.Property("vpeId", th.IntegerType),
        th.Property("vpeValue", th.NumberType),
        th.Property("specialOfferId", th.IntegerType),
        th.Property("_links", th.ObjectType(

            th.Property("taxClass", th.StringType)
        ))
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "product_id": record["id"],
        }

    
    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if previous_token is None:
            # If previous_token is None, we only got the first page so we should request page 2
            return 2

        # Parse response as JSON
        res = response.json()
        if len(res) == 0:
            # If this page was empty, we are done querying
            return None
        
        # time.sleep(1)
        # Else, query next page
        return previous_token + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""

        params: dict = {}
        if next_page_token is None:
            params["page"] = 1
        else:
            params["page"] = next_page_token

        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key

        params["per_page"] = 100
        
        return params


class ProductDetailStream(gambioStream):
        """Define custom stream."""
        name = "productsdetails"
        path = "/products/{product_id}"
        records_jsonpath = "$[*]"
        primary_keys = ["id"]
        replication_key = None
        parent_stream_type = ProductStream
        # Optionally, you may also use `schema_filepath` in place of `schema`:
        # schema_filepath = SCHEMAS_DIR / "users.json"

        def get_next_page_token(
            self, response: requests.Response, previous_token: Optional[Any]
        ) -> Optional[Any]:
            return None

        schema = th.PropertiesList(

            th.Property("id", th.IntegerType),
            th.Property("isActive", th.BooleanType),
            th.Property("sortOrder", th.IntegerType),
            th.Property("dateAdded", th.StringType),
            th.Property("dateAvailable", th.StringType),
            th.Property("lastModified", th.StringType),
            th.Property("orderedCount", th.IntegerType),
            th.Property("productModel", th.StringType),
            th.Property("ean", th.StringType),
            th.Property("price", th.NumberType),
            th.Property("discountAllowed", th.IntegerType),
            th.Property("taxClassId", th.IntegerType),
            th.Property("quantity", th.NumberType),
            th.Property("name", th.StringType),
            th.Property("image", th.StringType),
            th.Property("imageAltText", th.StringType),
            th.Property("urlKeywords", th.StringType),
            th.Property("weight", th.NumberType),
            th.Property("shippingCosts", th.NumberType),
            th.Property("shippingTimeId", th.IntegerType),
            th.Property("productTypeId", th.IntegerType),
            th.Property("manufacturerId", th.IntegerType),
            th.Property("quantityUnitId", th.IntegerType),
            th.Property("isFsk18", th.BooleanType),
            th.Property("isVpeActive", th.BooleanType),
            th.Property("vpeId", th.IntegerType),
            th.Property("vpeValue", th.NumberType),
            th.Property("specialOfferId", th.IntegerType),
            th.Property("name", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("description", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("keywords", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("metaTitle", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("metaDescription", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("metaKeywords", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("infoUrl", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("url", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("urlKeywords", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("checkoutInformation", th.ObjectType(

                th.Property("en", th.StringType),
                th.Property("de", th.StringType)

            )),
            th.Property("viewedCount", th.ObjectType(

                th.Property("en", th.IntegerType),
                th.Property("de", th.IntegerType)

            )),
            th.Property("images", th.ArrayType(
                th.ObjectType(

                    th.Property("filename", th.StringType),
                    th.Property("isPrimary", th.BooleanType),
                    th.Property("isVisible", th.BooleanType),
                    th.Property("imageAltText", th.ObjectType(

                            th.Property("en", th.StringType),
                            th.Property("de", th.StringType)

                        ))
                )

            )),
            th.Property("settings", th.ObjectType(

                th.Property("detailsTemplate", th.StringType),
                th.Property("optionsDetailsTemplate", th.StringType),
                th.Property("optionsListingTemplate", th.StringType),
                th.Property("showOnStartpage", th.BooleanType),
                th.Property("showQuantityInfo", th.BooleanType),
                th.Property("showWeight", th.BooleanType),
                th.Property("showPriceOffer", th.BooleanType),
                th.Property("showAddedDateTime", th.BooleanType),
                th.Property("priceStatus", th.IntegerType),
                th.Property("minOrder", th.IntegerType),
                th.Property("graduatedQuantity", th.IntegerType),
                th.Property("onSitemap", th.BooleanType),
                th.Property("sitemapPriority", th.StringType),
                th.Property("sitemapChangeFrequency", th.StringType),
                th.Property("propertiesDropdownMode", th.StringType),
                th.Property("startpageSortOrder", th.IntegerType),
                th.Property("showPropertiesPrice", th.BooleanType),
                th.Property("propertiesCombisQuantityCheckMode", th.IntegerType),
                th.Property("usePropertiesCombisShippingTime", th.BooleanType),
                th.Property("usePropertiesCombisWeight", th.BooleanType),
                th.Property("startpageSortOrder", th.IntegerType),
            )),
            th.Property("_links", th.ObjectType(

                th.Property("taxClass", th.StringType)

            ))

        
        ).to_dict()


class CatagoriesStream(gambioStream):
    """Define custom stream."""
    name = "categories"
    path = "/categories/"
    records_jsonpath = "$[*]"
    primary_keys = ["id"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
    
    th.Property("id", th.IntegerType),
    th.Property("parentId", th.IntegerType),
    th.Property("isActive", th.BooleanType), 
    th.Property("name", th.StringType),
    th.Property("headingTitle", th.StringType),
    th.Property("description", th.StringType),
    th.Property("urlKeywords", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "category_id": record["id"],
        }
    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if previous_token is None:
            # If previous_token is None, we only got the first page so we should request page 2
            return 2

        # Parse response as JSON
        res = response.json()
        if len(res) == 0:
            # If this page was empty, we are done querying
            return None
        
        # time.sleep(1)
        # Else, query next page
        return previous_token + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""

        params: dict = {}
        if next_page_token is None:
            params["page"] = 1
        else:
            params["page"] = next_page_token

        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key

        params["per_page"] = 100
        
        return params





class CatagoriesProductStream(gambioStream):
    """Define custom stream."""
    name = "categories_products"
    path = "/categories/{category_id}/products"
    records_jsonpath = "$[*]"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = CatagoriesStream
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    catagories_id = None;
    stream_called = False;

    def get_next_page_token(
            self, response: requests.Response, previous_token: Optional[Any]
        ) -> Optional[Any]:
            return None


    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""

        # getting catagories ID in response
        pars_url = self.get_url(context).split('/')
        if "categories" in pars_url and "products" in pars_url:
            self.catagories_id = pars_url[-2]
            self.stream_called = True
        else:
            self.stream_called = False

        
        params: dict = {}
        if next_page_token is None:
            params["page"] = 1
        else:
            params["page"] = next_page_token

        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key

        params["per_page"] = 100
        
        return params

    
    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        # TODO: Delete this method if not needed.
        if self.stream_called:
            row["catagories_id"] = self.catagories_id

        return row

    schema = th.PropertiesList(
        
        th.Property("category_id", th.IntegerType),      
        th.Property("id", th.IntegerType),
        th.Property("isActive", th.BooleanType),
        th.Property("sortOrder", th.IntegerType),
        th.Property("dateAdded", th.StringType),
        th.Property("dateAvailable", th.StringType),
        th.Property("lastModified", th.StringType),
        th.Property("orderedCount", th.IntegerType),
        th.Property("productModel", th.StringType),
        th.Property("ean", th.StringType),
        th.Property("price", th.NumberType),
        th.Property("discountAllowed", th.IntegerType),
        th.Property("taxClassId", th.IntegerType),
        th.Property("quantity", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("image", th.StringType),
        th.Property("imageAltText", th.StringType),
        th.Property("urlKeywords", th.StringType),
        th.Property("weight", th.NumberType),
        th.Property("shippingCosts", th.NumberType),
        th.Property("shippingTimeId", th.IntegerType),
        th.Property("productTypeId", th.IntegerType),
        th.Property("manufacturerId", th.IntegerType),
        th.Property("quantityUnitId", th.IntegerType),
        th.Property("isFsk18", th.BooleanType),
        th.Property("isVpeActive", th.BooleanType),
        th.Property("vpeId", th.IntegerType),
        th.Property("vpeValue", th.NumberType),
        th.Property("specialOfferId", th.IntegerType),
        th.Property("_links", th.ObjectType(

            th.Property("taxClass", th.StringType)

        ))

       
    ).to_dict()





"""
For Now I am putting Order Histroy Inside the comments
May be in furture we can reuse it if needed.
"""

# class OrdersHistoryStream(gambioStream):
#     """Define custom stream."""
#     name = "orders_history"
#     path = "/orders_history/{order_id}"

#     records_jsonpath = "$[*]"
#     primary_keys = ["id"]
#     replication_key = None
#     parent_stream_type = OrderList
#     # Optionally, you may also use `schema_filepath` in place of `schema`:
#     # schema_filepath = SCHEMAS_DIR / "users.json"
#     schema = th.PropertiesList(

#         th.Property("id", th.IntegerType),
#         th.Property("comment", th.StringType),
#         th.Property("customerNotified", th.BooleanType),
#         th.Property("dateAdded", th.StringType),
#         th.Property("statusId", th.IntegerType),
#         th.Property("_links", th.ObjectType(

#             th.Property("status", th.StringType)

#         ))
        
       
#     ).to_dict()



class ShopInformationStream(gambioStream):
    """Define custom stream."""
    name = "shop_information"
    path = "/shop_information"
    records_jsonpath = "$[*]"
    primary_keys = None
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"

    def get_next_page_token(
            self, response: requests.Response, previous_token: Optional[Any]
        ) -> Optional[Any]:
            return None


    schema = th.PropertiesList(

        th.Property("city", th.StringType),
        th.Property("company", th.StringType),
        th.Property("countryId", th.StringType),
        th.Property("email", th.StringType),
        th.Property("fax", th.StringType),
        th.Property("firstname", th.StringType),
        th.Property("houseNumber", th.StringType),
        th.Property("lastname", th.StringType),
        th.Property("owner", th.StringType),
        th.Property("postcode", th.StringType),
        th.Property("shopName", th.StringType),
        th.Property("shopVersion", th.StringType),
        th.Property("state", th.StringType),
        th.Property("street", th.StringType),
        th.Property("telephone", th.StringType),
        th.Property("url", th.StringType),
        th.Property("zoneId", th.StringType)
       
    ).to_dict()


class SpecialOffersStream(gambioStream):
    """Define custom stream."""
    name = "special_offers"
    path = "/special_offers"
    records_jsonpath = "$[*]"
    primary_keys = ["id"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(

        th.Property("id", th.IntegerType),
        th.Property("price", th.NumberType),
        th.Property("quantity", th.NumberType),
        th.Property("status", th.BooleanType),
        th.Property("beginsAt", th.StringType),
        th.Property("expiresAt", th.StringType),
        th.Property("productId", th.IntegerType),
        th.Property("added", th.StringType),
        th.Property("modified", th.StringType)
       
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if previous_token is None:
            # If previous_token is None, we only got the first page so we should request page 2
            return 2

        # Parse response as JSON
        res = response.json()
        if len(res) == 0:
            # If this page was empty, we are done querying
            return None
        
        # time.sleep(1)
        # Else, query next page
        return previous_token + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""

        params: dict = {}
        if next_page_token is None:
            params["page"] = 1
        else:
            params["page"] = next_page_token

        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key

        params["per_page"] = 100
        
        return params

