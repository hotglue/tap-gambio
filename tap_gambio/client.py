"""REST client handling, including gambioStream base class."""

from ast import Try
from typing_extensions import Self
import requests
from pathlib import Path
from typing import Any,Callable, Dict, Optional, Union, List, Iterable
import backoff
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
import singer
import logging
import time
 

from memoization import cached

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import BearerTokenAuthenticator

LOGGER = singer.get_logger()
logging.getLogger('backoff').setLevel(logging.CRITICAL)

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class gambioStream(RESTStream):
    """gambio stream class."""

    # url_base = "https://api.mysample.com"



    records_jsonpath = "$[*]"  # Or override `parse_response`.
    # next_page_token_jsonpath = "$.next_page"  # Or override `get_next_page_token`.

    # catagories_id = None;
    # stream_called = False;

    # OR use a dynamic url_base:
    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return str(self.config.get("api_url")) + "/api.php/v2"    


    @property
    def authenticator(self) -> BearerTokenAuthenticator:
        """Return a new authenticator object."""
        return BearerTokenAuthenticator.create_for_stream(
            self,
            token=self.config.get("api_key")
        )
    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        # If not using an authenticator, you may also provide inline auth headers:
        # headers["Private-Token"] = self.config.get("auth_token")
        return headers

    # def get_next_page_token(
    #     self, response: requests.Response, previous_token: Optional[Any]
    # ) -> Optional[Any]:
    #     """Return a token for identifying next page or None if no more pages."""
    #     if previous_token is None:
    #         # If previous_token is None, we only got the first page so we should request page 2
    #         return 2

    #     # Parse response as JSON
    #     res = response.json()
    #     if len(res) == 0:
    #         # If this page was empty, we are done querying
    #         return None
        
    #     # time.sleep(1)
    #     # Else, query next page
    #     return previous_token + 1


    # def get_url_params(
    #     self, context: Optional[dict], next_page_token: Optional[Any]
    # ) -> Dict[str, Any]:
    #     """Return a dictionary of values to be used in URL parameterization."""

    #     # # getting catagories ID in response
    #     # pars_url = self.get_url(context).split('/')
    #     # if "categories" in pars_url and "products" in pars_url:
    #     #     self.catagories_id = pars_url[-2]
    #     #     self.stream_called = True
    #     # else:
    #     #     self.stream_called = False

    #     params: dict = {}
    #     if next_page_token is None:
    #         params["page"] = 1
    #     else:
    #         params["page"] = next_page_token

    #     if self.replication_key:
    #         params["sort"] = "asc"
    #         params["order_by"] = self.replication_key

    #     params["per_page"] = 1000
        
    #     return params

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the REST API request.

        By default, no payload will be sent (return None).
        """
        # TODO: Delete this method if no payload is required. (Most REST APIs.)
        return None

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        # TODO: Parse response body and return a set of records.
       
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        # TODO: Delete this method if not needed.
        # if self.stream_called:
        #     row["catagories_id"] = self.catagories_id

        return row
    
    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response.

        By default, checks for error status codes (>400) and raises a
        :class:`singer_sdk.exceptions.FatalAPIError`.

        Tap developers are encouraged to override this method if their APIs use HTTP
        status codes in non-conventional ways, or if they communicate errors
        differently (e.g. in the response body).

        .. image:: ../images/200.png


        In case an error is deemed transient and can be safely retried, then this
        method should raise an :class:`singer_sdk.exceptions.RetriableAPIError`.

        Args:
            response: A `requests.Response`_ object.

        Raises:
            FatalAPIError: If the request is not retriable.
            RetriableAPIError: If the request is retriable.

        .. _requests.Response:
            https://docs.python-requests.org/en/latest/api/#requests.Response
        """

        if response.status_code == 500:
            return None

        if 400 <= response.status_code < 500:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise FatalAPIError(msg)

        elif 500 <= response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise RetriableAPIError(msg)

        
    def log_backoff_attempt(self, details):
        LOGGER.info("ConnectionFailure detected, triggering backoff: %d try", details.get("tries"))
        
    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures.

        Developers may override this method to provide custom backoff or retry
        handling.

        Args:
            func: Function to decorate.

        Returns:
            A decorated method.
        """
        
        decorator: Callable = backoff.on_exception(
        backoff.expo,
        (
            RetriableAPIError,
            FatalAPIError,
            requests.exceptions.ReadTimeout,
            requests.exceptions.RequestException,
            requests.exceptions.Timeout,
            # requests.exceptions.ConnectionError
        ),
        max_tries=10,
        factor=2,
        max_time=600,
        on_backoff= self.log_backoff_attempt
        )(func)
        return decorator
